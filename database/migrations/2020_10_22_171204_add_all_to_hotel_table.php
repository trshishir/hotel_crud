<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAllToHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hotel_details', function (Blueprint $table) {
            $table->string('logo')->nullable();
            $table->integer('location_id')->default(1);
            $table->string('address')->nullable();
            $table->string('mobile_number')->nullable();
            $table->string('rafered_by')->nullable();
            $table->integer('owner')->default(1);
            $table->string('tread_license')->nullable();
            $table->integer('visitor_count')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hotel_details', function (Blueprint $table) {
            //
        });
    }
}
