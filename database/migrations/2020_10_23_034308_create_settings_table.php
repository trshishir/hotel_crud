<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->default('Hotel');
            $table->string('logo')->default('logo.png');
            $table->string('fav')->default('default.png');
            $table->string('motto')->default('Best hotel booking place');

            $table->string('meta_description')->default('Best hotel booking place');
            $table->string('meta_author')->default('Hotel');

            $table->integer('reset_sms_count')->nullable()->default(3)->comment('Per day available reset message');
            $table->string('reset_sms_template')->nullable()->default('আপনার নতুন পাসওয়ার্ডঃ ')->comment('Password sms template');
            $table->string('welcome_sms_template')->nullable()->default('স্বাগতম')->comment('Welcome sms template');

            $table->text('about')->nullable('About description ....');
            $table->text('terms_and_condition')->nullable('Terms and conditions description ....');
            $table->text('privacy_policy')->nullable('Privacy and policy description ....');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
