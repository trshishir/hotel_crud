<?php

namespace App\Http\Controllers;

use App\Blog;
use App\hotel_details;
use App\Setting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FrontendController extends Controller
{
    //Frontend home page
    public function index(){
        $setting = Setting::find(1);
        $hotels = hotel_details::orderBy('id', 'desc')->take(9);
        return view('index',compact('setting', 'hotels') );
    }

    //Frontend blog page
    public function blog(){
        $setting = Setting::find(1);
        $blogs = Blog::orderBy('id', 'desc');
        return view('blog',compact('setting', 'blogs') );
    }
}
