@extends('layouts.app')
@push('title') Blog @endpush
@section('content')
    <div class="container">
        <div class="row htlfndr-page-content">
            <main id="htlfndr-main-content" class="col-sm-12 col-md-8 col-lg-9" role="main">
                <article class="post htlfndr-post">
                    <header class="htlfndr-entry-header">
                        <a href="#"><h2 class="htlfndr-entry-title">Amazing Standard Blog Post</h2></a>
                        <div class="htlfndr-entry-meta">
                            <span class="htlfndr-posted-by">Posted by <a href="#">Frank Mills</a></span>
                            <span class="htlfndr-post-date"><a href="#"><time datetime="2015-08-25">23 July 2015</time></a></span>
                            <span class="htlfndr-post-time"><a href="#"><time datetime="18:00">at 06 : 00 PM</time></a></span>
                            <span class="htlfndr-category-link"><a href="#">travel stories</a></span>
                            <span class="htlfndr-post-comments"><a href="#">24</a> Comments</span>
                        </div><!-- .htlfndr-entry-meta -->
                    </header>
                    <a href="#" class="htlfndr-post-thumbnail">
                        <img src="{{ asset('frontend-assets/images/29572el_chaise_longue_o_cheslon.jpg') }}" alt="post image" />
                    </a>
                    <div class="htlfndr-entry-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dignissimos dolorum ducimus modi neque quibusdam rerum similique velit? Aspernatur culpa delectus dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente. dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente.</p>
                        <a href="#" class="htlfndr-more-link">read more</a>
                    </div><!-- .htlfndr-entry-content -->
                </article><!-- .post .htlfndr-post -->

                <article class="post htlfndr-post htlfndr-slider-post">
                    <header class="htlfndr-entry-header">
                        <a href="blog-single.html"><h2 class="htlfndr-entry-title">Slider Blog Post</h2></a>
                        <div class="htlfndr-entry-meta">
                            <span class="htlfndr-posted-by">Posted by <a href="#">Sara Mills</a></span>
                            <span class="htlfndr-post-date"><a href="#"><time datetime="2015-08-25">23 July 2015</time></a></span>
                            <span class="htlfndr-post-time"><a href="#"><time datetime="18:00">at 02 : 00 PM</time></a></span>
                            <span class="htlfndr-category-link"><a href="#">travel stories</a></span>
                            <span class="htlfndr-post-comments"><a href="#">18</a> comments</span>
                        </div><!-- .htlfndr-entry-meta -->
                    </header>
                    <div class="owl-carousel htlfndr-post-thumbnail">
                        <div class="htlfndr-post-slide-wrapper">
                            <img src="{{ asset('frontend-assets/images/kitchen-island1.jpg') }}" alt="room picture" />
                        </div><!-- .htlfndr-post-slide-wrapper -->
                        <div class="htlfndr-post-slide-wrapper">
                            <img src="{{ asset('frontend-assets/images/5053563227_ee0db21ca5_b.jpg') }}" alt="room picture" />
                        </div><!-- .htlfndr-post-slide-wrapper -->
                        <div class="htlfndr-post-slide-wrapper">
                            <img src="{{ asset('frontend-assets/images/83890-Web1.jpg') }}" alt="room picture" />
                        </div><!-- .htlfndr-post-slide-wrapper -->
                        <div class="htlfndr-post-slide-wrapper">
                            <img src="{{ asset('frontend-assets/images/top-destination-1.jpg') }}" alt="room picture" />
                        </div><!-- .htlfndr-post-slide-wrapper -->
                        <div class="htlfndr-post-slide-wrapper">
                            <img src="{{ asset('frontend-assets/images/29572el_chaise_longue_o_cheslon.jpg') }}" alt="room picture" />
                        </div><!-- .htlfndr-slide-wrapper -->
                    </div><!-- .htlfndr-slider-post-slider -->
                    <div class="htlfndr-entry-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dignissimos dolorum ducimus modi neque quibusdam rerum similique velit? Aspernatur culpa delectus dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente. dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente.</p>
                        <a href="#" class="htlfndr-more-link">read more</a>
                    </div><!-- .htlfndr-entry-content -->
                </article><!-- .post .htlfndr-post .htlfndr-slider-post -->

                <!-- <article class="post htlfndr-post htlfndr-post-format-quote">
                    <header class="htlfndr-entry-header">
                        <a href="blog-single.html"><h2 class="htlfndr-entry-title">Quote Post Type</h2></a>
                        <div class="htlfndr-entry-meta">
                            <span class="htlfndr-posted-by">Posted by <a href="#">Admin</a></span>
                            <span class="htlfndr-post-date"><a href="#"><time datetime="2015-08-25">23 July 2015</time></a></span>
                            <span class="htlfndr-post-time"><a href="#"><time datetime="18:00">at 07 : 00 PM</time></a></span>
                            <span class="htlfndr-category-link"><a href="#">services</a></span>
                            <span class="htlfndr-post-comments"><a href="#">24</a> comments</span>
                        </div>
                    </header>
                    <div class="htlfndr-entry-content">
                        <blockquote>
                            <p>Quam habitasse odio habitasse ultrices dis varius ultrices imperdiet aliquam aliquam etiam</p>
                        </blockquote>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dignissimos dolorum ducimus modi neque quibusdam rerum similique velit? Aspernatur culpa delectus dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente. dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente.</p>
                        <a href="#" class="htlfndr-more-link">read more</a>
                    </div>
                </article>

                <article class="post htlfndr-post htlfndr-post-format-video">
                    <header class="htlfndr-entry-header">
                        <a href="blog-single.html"><h2 class="htlfndr-entry-title">Vimeo Video Blog Post</h2></a>
                        <div class="htlfndr-entry-meta">
                            <span class="htlfndr-posted-by">Posted by <a href="#">Frank Mills</a></span>
                            <span class="htlfndr-post-date"><a href="#"><time datetime="2015-08-25">23 July 2015</time></a></span>
                            <span class="htlfndr-post-time"><a href="#"><time datetime="18:00">at 06 : 00 PM</time></a></span>
                            <span class="htlfndr-category-link"><a href="#">trending now</a></span>
                            <span class="htlfndr-post-comments"><a href="#">24</a> Comments</span>
                        </div>
                    </header>
                    <div class="htlfndr-iframe-wrapper htlfndr-post-thumbnail">
                        <iframe src="http://placehold.it/825x465" frameborder="0"
                                        webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
                        <p><a href="#">The Hotel</a> from <a
                            href="#">HOMEMBALA</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
                    </div>
                    <div class="htlfndr-entry-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dignissimos dolorum ducimus modi neque quibusdam rerum similique velit? Aspernatur culpa delectus dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente. dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente.</p>
                        <a href="blog-single.html" class="htlfndr-more-link">read more</a>
                    </div>
                </article> -->

                <!-- <article class="post htlfndr-post htlfndr-post-format-video">
                    <header class="htlfndr-entry-header">
                        <a href="blog-single.html"><h2 class="htlfndr-entry-title">YouTube Blog Post</h2></a>
                        <div class="htlfndr-entry-meta">
                            <span class="htlfndr-posted-by">Posted by <a href="#">Frank Mills</a></span>
                            <span class="htlfndr-post-date"><a href="#"><time datetime="2015-08-25">23 July 2015</time></a></span>
                            <span class="htlfndr-post-time"><a href="#"><time datetime="18:00">at 06 : 00 PM</time></a></span>
                            <span class="htlfndr-category-link"><a href="#">trending now</a></span>
                            <span class="htlfndr-post-comments"><a href="#">24</a> Comments</span>
                        </div>
                    </header>
                    <div class="htlfndr-iframe-wrapper htlfndr-post-thumbnail">
                        <iframe src="http://placehold.it/820x465" frameborder="0" allowfullscreen></iframe>
                    </div>
                    <div class="htlfndr-entry-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dignissimos dolorum ducimus modi neque quibusdam rerum similique velit? Aspernatur culpa delectus dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente. dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente.</p>
                        <a href="#" class="htlfndr-more-link">read more</a>
                    </div>
                </article> -->

                <article class="post htlfndr-post">
                    <header class="htlfndr-entry-header">
                        <h2 class="htlfndr-entry-title">Simple Blog Post</h2>
                        <div class="htlfndr-entry-meta">
                            <span class="htlfndr-posted-by">Posted by <a href="#">Frank Mills</a></span>
                            <span class="htlfndr-post-date"><a href="#"><time datetime="2015-08-25">23 July 2015</time></a></span>
                            <span class="htlfndr-post-time"><a href="#"><time datetime="18:00">at 06 : 00 PM</time></a></span>
                            <span class="htlfndr-category-link"><a href="#">trending now</a></span>
                            <span class="htlfndr-post-comments"><a href="#">18</a> comments</span>
                        </div><!-- .htlfndr-entry-meta -->
                    </header>
                    <div class="htlfndr-entry-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. At dignissimos dolorum ducimus modi neque quibusdam rerum similique velit? Aspernatur culpa delectus dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente. dicta hic itaque laboriosam laudantium libero, perferendis quod sapiente.</p>
                        <a href="#" class="htlfndr-more-link">read more</a>
                    </div><!-- .htlfndr-entry-content -->
                </article><!-- .post .htlfndr-post -->

                <!-- Pagination -->
                <nav class="htlfndr-pagination">
                    <ul class="pagination">
                        <li class="htlfndr-left">
                            <a href="#" aria-label="Previous">
                                <span aria-hidden="true" class="fa fa-angle-left"></span>
                            </a>
                        </li>
                        <li class="current"><a href="#">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#">3</a></li>
                        <li><a href="#">4</a></li>
                        <li class="htlfndr-right">
                            <a href="#" aria-label="Next">
                                <span aria-hidden="true" class="fa fa-angle-right"></span>
                            </a>
                        </li>
                    </ul>
                </nav><!-- .htlfndr-pagination -->
            </main><!-- #htlfndr-main-content -->

            <aside id="htlfndr-right-sidebar" class="col-sm-12 col-md-4 col-lg-3 htlfndr-sidebar htlfndr-sidebar-in-right" role="complementary">
                <div class="widget">
                    <form action="#" class="htlfndr-blog-search-form" method="get" role="search">
                        <label for="htlfndr-blog-search-field" class="sr-only">Search for:</label>
                        <input type="search" title="Search for:" name="s" value="" placeholder="Search" id="htlfndr-blog-search-field" class="htlfndr-blog-search-field">
                        <input type="submit" value="search" class="htlfndr-blog-search-submit">
                    </form>
                </div><!-- .widget -->

                <div class="widget htlfndr-near-properties">
                    <div class="htlfndr-widget-main-content">
                        <h3 class="widget-title">properties	near</h3>
                        <div class="htlfdr-hotel-post">
                            <div class="htlfndr-post-inner htlfndr-table-view">
                                <div class="htlfndr-hotel-thumbnail">
                                    <a href="#">
                                        <img src="{{ asset('frontend-assets/images/83890-Web1.jpg') }}" alt="hotel pic"/>
                                    </a>
                                </div>
                                <!-- .htlfndr-hotel-thumbnail -->
                                <div class="htlfndr-hotel-info">
                                    <a href="hotel-page-v1.html"><h6>Ruzzini Palace Hotel</h6></a>
                                    <div class="htlfndr-rating-stars">
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                    </div><!-- .htlfndr-rating-stars -->
                                    <p class="htlfndr-hotel-price"><span>per night</span> <span class="htlfndr-cost-normal">$150</span>
                                    </p>
                                </div><!-- .htlfndr-hotel-info -->
                            </div><!-- .htlfndr-post-inner -->
                        </div><!-- .htlfdr-hotel-post -->

                        <div class="htlfdr-hotel-post">
                            <div class="htlfndr-post-inner htlfndr-table-view">
                                <div class="htlfndr-hotel-thumbnail">
                                    <a href="hotel-page-v1.html">
                                        <img src="{{ asset('frontend-assets/images/5053563227_ee0db21ca5_b.jpg') }}" alt="hotel pic"/>
                                    </a>
                                </div><!-- .htlfndr-hotel-thumbnail -->
                                <div class="htlfndr-hotel-info">
                                    <a href="hotel-page-v1.html"><h6>Foscari Palace</h6></a>
                                    <div class="htlfndr-rating-stars">
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div><!-- .htlfndr-rating-stars -->
                                    <p class="htlfndr-hotel-price"><span>per night</span> <span class="htlfndr-cost-normal">$200</span>
                                    </p>
                                </div><!-- .htlfndr-hotel-info -->
                            </div><!--.htlfndr-post-inner-->
                        </div><!-- .htlfdr-hotel-post -->

                        <div class="htlfdr-hotel-post">
                            <div class="htlfndr-post-inner htlfndr-table-view">
                                <div class="htlfndr-hotel-thumbnail">
                                    <a href="hotel-page-v1.html">
                                        <img src="{{ asset('frontend-assets/images/top-destination-2.jpg') }}" alt="hotel pic"/>
                                    </a>
                                </div><!-- .htlfndr-hotel-thumbnail -->
                                <div class="htlfndr-hotel-info">
                                    <a href="hotel-page-v1.html"><h6>Carnival Hotel</h6></a>
                                    <div class="htlfndr-rating-stars">
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star"></i>
                                    </div><!-- .htlfndr-rating-stars -->
                                    <p class="htlfndr-hotel-price"><span>per night</span> <span class="htlfndr-cost-normal">$400</span>
                                    </p>
                                </div><!-- .htlfndr-hotel-info -->
                            </div><!-- .htlfndr-post-inner -->
                        </div><!-- .htlfdr-hotel-post -->

                        <div class="htlfdr-hotel-post">
                            <div class="htlfndr-post-inner htlfndr-table-view">
                                <div class="htlfndr-hotel-thumbnail">
                                    <a href="hotel-page-v1.html">
                                        <img src="{{ asset('frontend-assets/images/top-destination-2.jpg') }}" alt="hotel pic"/>
                                    </a>
                                </div><!-- .htlfndr-hotel-thumbnail -->
                                <div class="htlfndr-hotel-info">
                                    <a href="hotel-page-v1.html"><h6>Hilton Molino</h6></a>
                                    <div class="htlfndr-rating-stars">
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star htlfndr-star-color"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </div><!-- .htlfndr-rating-stars -->
                                    <p class="htlfndr-hotel-price"><span>per night</span> <span class="htlfndr-cost-normal">$350</span>
                                    </p>
                                </div><!-- .htlfndr-hotel-info -->
                            </div><!-- .htlfndr-post-inner -->
                        </div><!-- .htlfdr-hotel-post -->
                    </div><!-- .htlfndr-widget-main-content .htlfndr-widget-padding -->
                </div><!-- .widget .htlfndr-near-properties -->

                <div class="widget htlfndr-widget-archive">
                    <div class="htlfndr-widget-main-content htlfndr-widget-padding">
                        <h3 class="widget-title">archives</h3>
                        <ul>
                            <li><a href="#">august 2015</a></li>
                            <li><a href="#">july 2015</a></li>
                            <li><a href="#">june 2015</a></li>
                            <li><a href="#">may 2015</a></li>
                            <li><a href="#">april 2015</a></li>
                            <li><a href="#">march 2015</a></li>
                            <li><a href="#">february 2015</a></li>
                            <li><a href="#">january 2015</a></li>
                            <li><a href="#">december 2014</a></li>
                        </ul>
                    </div><!-- .htlfndr-widget-main-content .htlfndr-widget-padding -->
                </div><!-- .widget .htlfndr-widget-archive -->

                <div class="widget htlfndr-widget-category">
                    <div class="htlfndr-widget-main-content htlfndr-widget-padding">
                        <h3 class="widget-title">categories</h3>
                        <ul>
                            <li><a href="#">photos</a></li>
                            <li><a href="#">travel stories</a></li>
                            <li><a href="#">trending now</a></li>
                            <li><a href="#">places to go</a></li>
                            <li><a href="#">services</a></li>
                        </ul>
                    </div>
                </div>

                <!-- <div class="widget htlfndr-widget-twitter">
                    <div class="htlfndr-widget-main-content htlfndr-widget-padding">
                        <h3 class="widget-title">twitter feed</h3>
                        <ul class="htlfndr-recent-tweets">
                            <li>
                                Very comfortable and beautiful theme <a href="#" class="hashtag">#HotelFinder</a> <a href="#" class="hashtag">#themeforest</a> by <a
                                href="#">@bestwebsoft</a>
                                <p class="htlfndr-twitter-time"><a href="#">3 days ago</a></p>
                            </li>
                            <li>
                                Connecting <a href="#">buff.ly/1oSET13</a> great <a href="#" class="hashtag">#documentary</a> on <a
                                href="#" class="hashtag">#interaction</a> <a href="#" class="hashtag">#design</a> by <a href="#">@Microsoft</a>
                                <p class="htlfndr-twitter-time"><a href="#">5 days ago</a></p>
                            </li>
                            <li>
                                <a href="#">@hotelfinder</a> Wordpress Booking Theme is launched! <a href="#" class="hashtag">#wordPress</a>
                                <a href="#" class="hashtag">#bestwebsoft</a> <a href="#" class="hashtag">#new</a>
                                <p class="htlfndr-twitter-time"><a href="#">12 days ago</a></p>
                            </li>
                        </ul>
                        <a class="twitter-timeline"  href="https://twitter.com/Wishmaker88" data-widget-id="636886660064649216"
                             data-chrome="noheader noborders noscrollbar nofooter" data-tweet-limit="3" data-screen-name="bestwebsoft"
                             data-show-replies="false">Tweets from @bestwebsoft</a> -->

                <!-- <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+"://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script> -->


                <!-- </div>
            </div> -->

                <div class="widget htlfndr-widget-help">
                    <div class="htlfndr-widget-main-content htlfndr-widget-padding">
                        <h3 class="widget-title">need our help</h3>
                        <span>24/7 dedicated customer support</span>
                        <p class="htlfndr-phone">+(000) 000-000-000</p>
                        <p class="htlfndr-mail">support@bestwebsoft.zendesk.com</p>
                    </div><!-- .htlfndr-widget-main-content .htlfndr-widget-padding -->
                </div><!-- .widget .htlfndr-widget-help -->
            </aside><!-- .htlfndr-sidebar-in-right -->
        </div><!-- .row .htlfndr-page-content -->
    </div>
@endsection
